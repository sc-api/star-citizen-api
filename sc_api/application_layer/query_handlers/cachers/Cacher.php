<?php

namespace thalos_api;

require_once(__DIR__.'/../../../database_layer/DBInterface.php');

abstract class Cacher
{
    protected $Output;
    protected $DB;
    protected $functions_array;
    protected $query_chain_entry;
    
    public function __construct($Output, $query_chain_parent = null)
    {
        $this->Output = $Output;
        $this->functions_array = array();
        
        // Grab a database connection
        $this->DB = new \thalos_api\DBInterface();
        $this->DB->Connect();
        
        // Perform any specific settings validation
        $this->ValidateLocalSettings();
        
        $name = Controller::parse_classname(get_class($this));
        
        if($query_chain_parent != null)
        {
            $this->query_chain_entry = new QueryChain(array('name'=>$name['classname']));
            $query_chain_parent->children[] = $this->query_chain_entry;
        }
    }
    
    public function PerformQuery()
    {
        unset($this->Output->data['entry_id']);
        //unset($this->output['data']['last_scrape_date']);
        unset($this->Output->data['scrape_date']);
        
        // If we gathered any data
        if(isset($this->Output->data))
        {
            // For each of the supplied patterns
            foreach($this->functions_array as $index=>$match)
            {
                // Apply any specified filters on the returned data
                $this->Output->data[$match->name] = $this->ApplyPostprocessing($match->postprocesses, $this->Output->data[$match->name]);
            }
        }
    }
    
    protected abstract function ValidateLocalSettings();
    
    private function ApplyPostprocessing($functions_array, $input)
    {
        if(isset($functions_array)
            && $functions_array != null)
        {
            // For each of the filters requested
            foreach($functions_array as $function)
            {
                $params = array();
                
                // For each of the parameters in this function
                foreach($function->arguments as $param)
                {
                    // If the user requested this paramter be our matched data
                    if($param == '$?')
                    {
                        $params[] = $input;
                    }
                    // Else, use whatever the user supplied
                    else
                    {
                        $params[] = $param;
                    }
                }
                
                // Run the function
                $input = @call_user_func_array($function->callback, $params);
            }
        }
        
        // Return the item after all filters
        return $input;
    }
    
    public abstract function UpdateCache($target, $sender);
    
    public static function UpdateScrapeAttempt($target, $table)
    {
        // Grab a database connection
        $DB = new \thalos_api\DBInterface();
        $DB->Connect();
        
        if($DB->CheckReference($table, $target))
        {
            $query=$DB->db->prepare(
                'UPDATE 
                    '.$table.'
                SET
                    last_scrape_date = :time
                WHERE 
                    id = :target');
            
            $time = time();
        
            $query->bindParam(':target', $target);
            $query->bindParam(':time', $time);

            $query->execute();
        }
    }
}