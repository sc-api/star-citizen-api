<?php

namespace thalos_api;

require_once(__DIR__.'/../Cacher.php');

class RSIAllOrgCacher extends Cacher
{
    
    public function PerformQuery()
    {
        $result = $this->DB->GetTargetRows(TableDirectory::RSIOrgsTable, null, time());
        
        if(count($result) > 0)
        {
            $this->Output->SetData($result);
        }
        else
        {
            $this->Output->data = null;
        }
        
        return parent::PerformQuery();
    }
    
    protected function ValidateLocalSettings()
    { }
    
    public function UpdateCache($targets, $sender)
    {
        $class = Controller::parse_classname(get_class($sender));
        
        foreach($targets as $target)
        {
            if($this->DB->CheckReference(TableDirectory::RSIOrgsTable, $target))
            {
                $query=$this->DB->db->prepare(
                        'UPDATE 
                            '.TableDirectory::RSIOrgsTable.'
                        SET
                            last_scrape_date = :time
                        WHERE 
                            id = :target');
            }
            else
            {
                $query=$this->DB->db->prepare(
                        'INSERT INTO 
                            '.TableDirectory::RSIOrgsTable.'
                            (
                                id,
                                date_added,
                                last_scrape_date
                            )
                        VALUES
                            (
                                :target,
                                :time,
                                :time
                            )');
            }

            $query->bindParam(':target', $target);

            // To appease the error output
            $time = time();
            $query->bindParam(':time', $time);

            $query->execute();
        }
    }
}