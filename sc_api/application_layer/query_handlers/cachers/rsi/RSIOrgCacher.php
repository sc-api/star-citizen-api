<?php

namespace thalos_api;

require_once(__DIR__.'/../Cacher.php');

class RSIOrgCacher extends Cacher
{
    public function PerformQuery()
    {
        $target_id = $this->Output->request_stats->resolved_query->target_id;
        
        $result = $this->DB->RebuildHistory(TableDirectory::RSIOrgsInfoTable, $target_id, time());
        
        $this->Output->SetData($result[0]);
        
        return parent::PerformQuery();
    }
    
    protected function ValidateLocalSettings()
    { }
    
    public function UpdateCache($target, $sender)
    {
        $class = Controller::parse_classname(get_class($sender));
        
        if($class['classname'] == RSIOrgScraper)
        {
            $this->UpdateOnSingle($target);
        }
        else if($class['classname'] == RSIAllOrgScraper)
        {
            $this->UpdateOnAll($target);
        }
        else if($class['classname'] == RSIDossierScraper)
        {
            $this->UpdateOnSingle($target);
        }
        
        if($this->DB->CheckReference(TableDirectory::RSIOrgsTable, $target))
        {
            $query=$this->DB->db->prepare(
                    'UPDATE 
                        '.TableDirectory::RSIOrgsTable.'
                    SET
                        last_scrape_success = :time
                    WHERE 
                        id = :target');
        }
        else
        {
            $query=$this->DB->db->prepare(
                    'INSERT INTO 
                        '.TableDirectory::RSIOrgsTable.'
                        (
                            id,
                            date_added,
                            last_scrape_success
                        )
                    VALUES
                        (
                            :target,
                            :time,
                            :time
                        )');
        }

        $time = time();
        
        $query->bindParam(':target', $target);
        $query->bindParam(':time', $time);

        $query->execute();
    }
    
    private function UpdateOnSingle($target)
    {
        $data = $this->DB->Diff(TableDirectory::RSIOrgsInfoTable, $target, $this->Output->data);
        
        $do_update = false;
        foreach($data as $key=>$item)
        {
            if($item != ''
                && $key!='sid'
                && $key!='scrape_date'
                && $key!='logo')
            {
                $do_update = true;
            }
        }
        
        if($do_update)
        {
            $data['sid'] = $target;
            $data['scrape_date'] = time();
            
            $table_cols = array(
                'scrape_date',
                'sid',
                'banner',
                'logo',
                'member_count',
                'title',
                'archetype',
                'commitment',
                'roleplay',
                'primary_focus',
                'primary_image',
                'secondary_focus',
                'secondary_image',
                'headline',
                'history',
                'manifesto',
                'charter',
                'recruiting',
                'cover_image',
                'cover_video',
                );
            
            $query = $this->DB->BuildInsertQuery(TableDirectory::RSIOrgsInfoTable, $table_cols);
            
            foreach($table_cols as $item)
            {
                $query->bindParam(':'.$item, $data[$item]);
            }

            $query->execute();
        }
    }
    
    private function UpdateOnAll($target)
    {
        $data = $this->DB->Diff(TableDirectory::RSIOrgsInfoTable, $target, $this->Output->data);
        
        $do_update = false;
        foreach($data as $key=>$item)
        {
            if($item != ''
                && $key!='sid'
                && $key!='scrape_date'
                && $key!='logo')
            {
                $do_update = true;
            }
        }
        
        if($do_update)
        {
            $data['sid'] = $target;
            $data['scrape_date'] = time();
            
            $table_cols = array(
                'scrape_date',
                'sid',
                'logo',
                'member_count',
                'title',
                'archetype',
                'commitment',
                'recruiting',
                'roleplay',
                'lang',
                );
            
            $query = $this->DB->BuildInsertQuery(TableDirectory::RSIOrgsInfoTable, $table_cols);
            
            foreach($table_cols as $item)
            {
                $query->bindParam(':'.$item, $data[$item]);
            }

            $query->execute();
        }
    }
}