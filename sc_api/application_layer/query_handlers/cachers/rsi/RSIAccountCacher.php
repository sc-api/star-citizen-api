<?php

namespace thalos_api;

use PDO;

require_once(__DIR__.'/../Cacher.php');

class RSIAccountCacher extends Cacher
{
    public function __construct($Output, $query_chain_parent = null)
    {
        parent::__construct($Output, $query_chain_parent);
        
        $this->functions_array = array
        (
            new Item('forum_roles',null,array(new Postprocess('explode', array(',','$?')))),
            new Item('fluency',null,array(new Postprocess('explode', array(',','$?'))))
        );
    }
    
    public function PerformQuery()
    {
        $target_id = $this->Output->request_stats->resolved_query->target_id;
        
        $result = $this->DB->RebuildHistory(TableDirectory::RSIAccountsInfoTable, $target_id, time());
        
        if($result[0]['citizen_number'] != null)
        {
            $query_string = 'SELECT DISTINCT(sid) FROM '.TableDirectory::RSIOrgsMembersTable.' WHERE handle = :handle AND scrape_date <= :end_date';
            $query = $this->DB->db->prepare($query_string);
            $query->bindParam(':handle', $target_id);
            $query->bindParam(':end_date',$this->Output->request_stats->resolved_query->date_range->end_date);
            $query->execute();
            
            while($row = $query->fetch(PDO::FETCH_ASSOC))
            {
                list($org_history, $org_history_last_scrape) = 
                        $this->DB->RebuildHistory(TableDirectory::RSIOrgsMembersTable, $row['sid'], time(), $result[0]['handle']);
                
                //var_dump($org_history);
                //die();
                
                $result[0]['organizations'][] = array(
                    'sid'=>$org_history['sid'],
                    'rank'=>$org_history['rank'],
                    'stars'=>$org_history['stars'],
                    'roles'=>$org_history['roles'],);
            }

            $this->Output->SetData($result[0]);
            
//            if(isset($this->Output->data['organizations']))
//            {
//                foreach($this->Output->data['organizations'] as $index=>$org)
//                {
//                    unset($this->Output->data['organizations'][$index]['handle']);
//                }
//            }
        }
        else
        {
            $this->Output->data = null;
        }
        
        return parent::PerformQuery();
    }
    
    protected function ValidateLocalSettings()
    { }
    
    public function UpdateCache($target, $sender)
    {
        $class = Controller::parse_classname(get_class($sender));
        
        if($class['classname'] == RSIForumProfileScraper)
        {
            $this->UpdateOnProfile($target);
        }
        else if($class['classname'] == RSIDossierScraper)
        {
            $this->UpdateOnDossier($target);
        }
        else if($class['classname'] == RSIDossierOrgsScraper)
        {
            $this->UpdateOnOrgMember($target);
        }
        else if($class['classname'] == RSIOrgMembersScraper)
        {
            $this->UpdateOnOrgMembers($target);
        }
        
        if($this->Output->data != null)
        {
            if($this->DB->CheckReference(TableDirectory::RSIAccountsTable, $target))
            {
                $query=$this->DB->db->prepare(
                        'UPDATE 
                            '.TableDirectory::RSIAccountsTable.'
                        SET
                            last_scrape_success = :time
                        WHERE 
                            id = :target');
            }
            else
            {
                $query=$this->DB->db->prepare(
                        'INSERT INTO 
                            '.TableDirectory::RSIAccountsTable.'
                            (
                                id,
                                date_added,
                                last_scrape_success
                            )
                        VALUES
                            (
                                :target,
                                :time,
                                :time
                            )');
            }

            $time = time();

            $query->bindParam(':target', $target);
            $query->bindParam(':time', $time);

            $query->execute();
        }
    }
    
    private function UpdateOnDossier($target)
    {
        $this->Output->data['fluency'] = implode(',', $this->Output->data['fluency']);
        $data = $this->DB->Diff(TableDirectory::RSIAccountsInfoTable, $target, $this->Output->data);
        $this->Output->data['fluency'] = explode(',', $this->Output->data['fluency']);
        
        $do_update = false;
        foreach($data as $key=>$item)
        {
            if($item != ''
                && $key!='handle'
                && $key!='scrape_date'
                && $key!='organizations')
            {
                $do_update = true;
            }
        }
        
        if($do_update)
        {
            $data['handle'] = $target;
            $data['scrape_date'] = time();
            
            $table_cols = array(
                'scrape_date',
                'handle',
                'citizen_number',
                'avatar',
                'moniker',
                'enlisted',
                'title_image',
                'title',
                'bio',
                'website_link',
                'website_title',
                'country',
                'region',
                'fluency',
                );
            
            $query = $this->DB->BuildInsertQuery(TableDirectory::RSIAccountsInfoTable, $table_cols);
            
            foreach($table_cols as $item)
            {
                $query->bindParam(':'.$item, $data[$item], PDO::PARAM_NULL);
            }

            $query->execute();
        }
    }
    
    private function UpdateOnProfile($target)
    {
        $this->Output->data['forum_roles'] = implode(',', $this->Output->data['forum_roles']);
        $data = $this->DB->Diff(TableDirectory::RSIAccountsInfoTable, $target, $this->Output->data);
        $this->Output->data['forum_roles'] = explode(',', $this->Output->data['forum_roles']);
        
        $do_update = false;
        foreach($data as $key=>$item)
        {
            if($item != ''
                && $key!='post_count'
                && $key!='last_forum_visit'
                && $key!='handle'
                && $key!='scrape_date'
                && $key!='organizations')
            {
                $do_update = true;
            }
        }
        
        if($do_update)
        {
            $data['handle'] = $target;
            $data['scrape_date'] = time();
            
            $table_cols = array(
                'scrape_date',
                'handle',
                'status',
                'discussion_count',
                'post_count',
                'last_forum_visit',
                'forum_roles',
                );
            
            $query = $this->DB->BuildInsertQuery(TableDirectory::RSIAccountsInfoTable, $table_cols);
            
            foreach($table_cols as $item)
            {
                $query->bindParam(':'.$item, $data[$item], PDO::PARAM_NULL);
            }

            $query->execute();
        }
    }
    
    private function UpdateOnOrgMembers($target)
    {
        //$this->output['data']['roles'] = implode(',', $this->output['data']['roles']);
        $data = $this->DB->Diff(TableDirectory::RSIOrgsMembersTable, $this->Output->data['sid'], $this->Output->data, $target);
        
        $do_update = false;
        foreach($data as $key=>$item)
        {
            if($item != ''
                && $key!='sid'
                && $key!='handle'
                && $key!='scrape_date'
                && $key!='status')
            {
                $do_update = true;
            }
        }
        
        if($do_update)
        {
            $data['scrape_date'] = time();
            $data['date_added'] = time();
            $data['sid'] = $this->Output->data['sid'];
            $data['handle'] = $target;
            $data['status'] = 'active';
            
            $table_cols = array(
                'scrape_date',
                'date_added',
                'sid',
                'handle',
                'status',
                'roles',
                'rank',
                'stars',
                'type',
                'visibility',
                );
            
            $query = $this->DB->BuildInsertQuery(TableDirectory::RSIOrgsMembersTable, $table_cols);
            
            foreach($table_cols as $item)
            {
                $query->bindParam(':'.$item, $data[$item], PDO::PARAM_NULL);
            }

            $query->execute();
        }
    }
    
    private function UpdateOnOrgMember($target)
    {
        if($this->Output->data['organizations'] != null)
        {
            foreach($this->Output->data['organizations'] as $org)
            {
                $data = $this->DB->Diff(TableDirectory::RSIOrgsMembersTable, $org['sid'], $org, $target);

                $do_update = false;
                foreach($data as $key=>$item)
                {
                    if($item != ''
                        && $key!='sid'
                        && $key!='handle'
                        && $key!='scrape_date'
                        && $key!='status')
                    {
                        $do_update = true;
                    }
                }

                if($do_update)
                {
                    // Overrides
                    $data['scrape_date'] = time();
                    $data['date_added'] = time();
                    $data['sid'] = $org['sid'];
                    $data['handle'] = $target;
                    $data['status'] = 'active';

                    $table_cols = array(
                        'scrape_date',
                        'date_added',
                        'sid',
                        'handle',
                        'status',
                        'roles',
                        'rank',
                        'stars',
                        'type',
                        'visibility',
                        );

                    $query = $this->DB->BuildInsertQuery(TableDirectory::RSIOrgsMembersTable, $table_cols);

                    foreach($table_cols as $item)
                    {
                        $query->bindParam(':'.$item, $data[$item], PDO::PARAM_NULL);
                    }

                    $query->execute();
                }
            }
        }
    }
}